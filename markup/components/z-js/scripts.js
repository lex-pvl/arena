$(document).ready(function() {
	$('#marka').select2({
		minimumResultsForSearch: Infinity,
	});	

	$('#models').select2({
		minimumResultsForSearch: Infinity
	});	

	$('#period').select2({
		minimumResultsForSearch: Infinity
	});	

	$('#price').select2({
		minimumResultsForSearch: Infinity
	});	

	$('#dprice').select2({
		minimumResultsForSearch: Infinity
	});	

	$('#ddate').select2({
		minimumResultsForSearch: Infinity
	});	

	$('#dprobeg').select2({
		minimumResultsForSearch: Infinity
	});	

	$('#dmarka').select2({
		minimumResultsForSearch: Infinity,
	});

	$('#dmodel').select2({
		minimumResultsForSearch: Infinity,
	});

	$('#dperiod').select2({
		minimumResultsForSearch: Infinity,
	});

	$('#dprobeg2').select2({
		minimumResultsForSearch: Infinity,
	});

	$("#form-phone").mask("+7 999 - 999 - 99 - 99", {
		completed: function(){
			return +this.val().replace(/[^\d\.]/g, '')
		}
	});

	var mainSwiper = new Swiper('.main-container', {
		navigation: {
			nextEl: '.main-next',
			prevEl: '.main-prev',
		},
		pagination: {
			el: '.main-pagination',
		},
		autoplay: {
			delay: 3000
		}
	});

	var rewievsSwiper = new Swiper('.reviews-container', {
		slidesPerView: 3,
		spaceBetween: 24,
		autoHeight: false,
		navigation: {
			nextEl: '.reviews-next',
			prevEl: '.reviews-prev',
		},
		// autoplay: {
		// 	delay: 3000
		// },
		breakpoints: {
			320: {
				slidesPerView: 1,
				spaceBetween: 20,
				centeredSlides: true,
			},
			767: {
				slidesPerView: 1,
				spaceBetween: 40,
			},
			1249: {
				slidesPerView: 2,
				spaceBetween: 21,
			}
		}
	});

	function autoFor() {
		$('.car-label').each(function() {
			$(this).attr('for', $(this).prev().attr("id"));
		});
		$('.d-label').each(function() {
			$(this).attr('for', $(this).prev().attr("id"));
		});
	}

	autoFor();


	function toUp() {
		$(window).on('scroll', function() {
			if ( $(window).scrollTop() > 1000 ) {
				$('.to-up').addClass('show')
			} else {
				$('.to-up').removeClass('show')
			}
		});

		$('.to-up').on('click', function() {
			$('html,body').stop().animate({ scrollTop: 0 }, 2000);
		});
	}

	toUp();

	function responsive() {
		width = $(window).width();

		if (width <= 1750) {
			$('.header__logo').append('<span class="logo-span">|   автомобили с пробегом   |</span>')
		} 

		if (width <= 1249 && width >= 992) {
			$('.deal__variant-body').width( $('.deal__variant').width() * 2 - 40 );
			$('.sell-body').addClass('transform-body');
			$('ul.deal__variants').on('click', 'li:not(.active)', function() {
				$(this)
				.addClass('active').siblings().removeClass('active')
				.closest('div.deal').find('div.deal__variant-body').removeClass('active').eq($(this).index()).addClass('active');
			});

			$('.header__bottom-line').clone().prependTo('.mobile__menu .container');
			$('.open-menu').on('click', function() {
				$(this).toggleClass('active')
				$('body').toggleClass('no-scroll');
				$('.mobile__menu').toggleClass('active');
			});
		}

		if (width <= 1249) {
			$('.header__bottom-line').clone().prependTo('.mobile__menu .container');
			$('.open-menu').on('click', function() {
				$(this).toggleClass('active')
				$('body').toggleClass('no-scroll');
				$('.mobile__menu').toggleClass('active');
			});
		}

	}

	responsive();

	function initMap() {
		ymaps.ready(init);
		var iconUrl = $('#map').data('icon-url');

		function init() {
			var map = new ymaps.Map('map', {
				center: [55.19403397, 61.38209575],
				zoom: 17,
				controls: [],
				behaviors: ['drag']
			});

			var placemark = new ymaps.Placemark([55.19403290, 61.38209575], {
				hintContent: ''
			},
			{
				iconLayout: 'default#image',
				iconImageHref: iconUrl,
				iconImageSize: [36, 52]
			});

			map.geoObjects.add(placemark)
		}
	}
	initMap();
});